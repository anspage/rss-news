package com.tradevan.run;

import com.tradevan.crawler.BusinessNews;
import com.tradevan.crawler.ChinatimesNews;
import com.tradevan.crawler.ETtodayNews;
import com.tradevan.crawler.JokerNews;
import com.tradevan.crawler.LtnNews;
import com.tradevan.crawler.MoneyNews;
import com.tradevan.crawler.NewTalkNews;
import com.tradevan.crawler.PeopleNews;
//import com.tradevan.crawler.PeopleNews;
import com.tradevan.crawler.RssCnaNews;
import com.tradevan.crawler.StormNews;
import com.tradevan.crawler.UdnNews;
import com.tradevan.crawler.UpMediaNews;
import com.tradevan.operate.LogAction;
import com.tradevan.operate.RSScrawler;

public class RunCrawlers {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LogAction logAction = new LogAction();
		logAction.setLogget(RunCrawlers.class);
		int slt = Integer.parseInt(args[0]);
		RSScrawler rss = null;
		// that five web stand alone
		switch (slt) {
//		case 1:
//			rss = new AppleNews();
//			break;
		case 2:
			rss = new UdnNews();
			break;
		case 3:
			rss = new ChinatimesNews();
			break;
		case 4:
			rss = new LtnNews();
			break;
		case 5:
			rss = new NewTalkNews();
			break;
		case 6:
			rss = new RssCnaNews();
			break;
		case 7:
			rss = new ETtodayNews();
			break;
		case 8:
			rss = new UpMediaNews();
			break;
		case 9:
			rss = new StormNews();
			break;
		case 10:
			rss = new BusinessNews();
			break;
		case 11:
			rss = new MoneyNews();
			break;
		case 12:
			rss = new JokerNews();
			break;
		case 13:
			rss = new PeopleNews();
			break;
		default:

			break;
		}

		//
		// logAction.getLogger().info(
		// String.format("Goto %s", fdaItem.getInfo()));
		try {
			rss.run(Integer.parseInt(args[1]));
		} catch (Exception e) {
			rss.run();
		}
		// logAction.getLogger().info(
		// String.format("Finished %s", fdaItem.getInfo()));

	}

}
