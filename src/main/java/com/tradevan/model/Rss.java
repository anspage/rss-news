package com.tradevan.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Rss {
	private String time = "";
	private String r_content_full = "";
	private String category = "";
	private String source = "";
	private String description = "";
	private String link = "";
	private String title = "";
	private String p_time = "";
	private String update_time = "";

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getP_time() {
		SimpleDateFormat sdf = new SimpleDateFormat("EEE,dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
		Date date = null;
		try {
			date = sdf.parse(this.p_time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String result = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		return result;
	}

	public void setP_time(String p_time) {
		this.p_time = p_time;
	}

	public String getUpdate_time() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());
		String result = formatter.format(date);
		return result;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getPubDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("EEE,dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
		Date date = null;
		try {
			date = sdf.parse(this.time);
		} catch (ParseException e) {
//			e.printStackTrace();
			date = new Date(System.currentTimeMillis());
		}
		String result = new SimpleDateFormat("yyyy-MM-dd").format(date);
		return result;
	}

	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getR_content_full() {
		return this.r_content_full;
	}

	public void setR_content_full(String r_content_full) {
		this.r_content_full = r_content_full;
	}
}
