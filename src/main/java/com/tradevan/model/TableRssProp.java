package com.tradevan.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TableRssProp {

	private String tablename = "", title = "", content = "", post_date = "",
			url = "", source = "", category = "", full_content = "",p_date = "",update_date = "";

	public String getP_date() {
		return p_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public TableRssProp() {
		FileInputStream fis = null;
		String path = "";
		try {
//			path = this.getClass().getResource("/").getPath();
			path = path + "src/main/resource/table_rss.properties";
			fis = new FileInputStream(path);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			String read_line;

			try {
				while ((read_line = br.readLine()) != null) {
					if (read_line.indexOf("#") == 0) {
						continue;
					}
					if (read_line.toLowerCase().indexOf("tablename:") >= 0) {
						this.tablename = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("title:") >= 0) {
						this.title = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("content:") >= 0) {
						this.content = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("post_date:") >= 0) {
						this.post_date = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("url:") >= 0) {
						this.url = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("source:") >= 0) {
						this.source = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("category:") >= 0) {
						this.category = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("full_context:") >= 0) {
						this.full_content = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("p_date:") >= 0) {
						this.p_date = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf("update_date:") >= 0) {
						this.update_date = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getTablename() {
		return tablename;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public String getPost_date() {
		return post_date;
	}

	public String getUrl() {
		return url;
	}

	public String getSource() {
		return source;
	}

	public String getCategory() {
		return category;
	}

	public String getFull_content() {
		return full_content;
	}

}
