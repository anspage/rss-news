package com.tradevan.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class GeckoProp {

	private String driverPath = "";
	private int sleeptime = 0, limit = 0;

	public GeckoProp() {
		FileInputStream fis = null;
		try {
			String path = "";
//			path = this.getClass().getResource("/").getPath();
			path = path + "src/main/resource/gecko.properties";
			fis = new FileInputStream(path);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			String read_line;

			try {
				while ((read_line = br.readLine()) != null) {
					if (read_line.indexOf("#") == 0) {
						continue;
					}
					if (read_line.toLowerCase().indexOf(
							"driverPath:".toLowerCase()) >= 0) {
						this.driverPath = read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim();
					}
					if (read_line.toLowerCase().indexOf(
							"sleeptime:".toLowerCase()) >= 0) {
						this.sleeptime = Integer.parseInt(read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim());
					}
					if (read_line.toLowerCase().indexOf("limit:".toLowerCase()) >= 0) {
						this.limit = Integer.parseInt(read_line
								.substring(read_line.indexOf(":") + 1)
								.toLowerCase().trim());
					}

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getDriverPath() {
		return driverPath;
	}

	public int getSleeptime() {
		return sleeptime;
	}

	public int getLimit() {
		return limit;
	}

}
