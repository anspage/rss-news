package com.tradevan.operate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.tradevan.model.DBprop;

public class AccessDatabase extends DBprop {
	private static Logger log = Logger.getLogger(AccessDatabase.class);
	private Connection _con = null;

	public AccessDatabase() {
		try {
			Class.forName(super.getDriver());
			this._con = DriverManager.getConnection(
					String.format("jdbc:%s://%s:%s/%s?%s", super.getDriverType(), super.getHost(), super.getPort(),
							super.getDatabase(),
							"serverTimezone=UTC&useServerPrepStmts=false&rewriteBatchedStatements=true"),
					super.getUsername(), super.getPassword());

			// this._con = DriverManager
			// .getConnection(
			// String.format("jdbc:%s://%s:%s;DatabaseName=%s;",
			// super.getDriverType(), super.getHost(),
			// super.getPort(), super.getDatabase()
			// .toUpperCase()), super
			// .getUsername(), super.getPassword());

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// insert data into specific table
	public void write2Table(String insertSQL, Set<List<String>> setData) {
		PreparedStatement ps = null;
		int countBatch = 0;
		try {
			ps = this._con.prepareStatement(StringEscapeUtils.unescapeJava(insertSQL));
			int i = 1;
//			System.out.println(StringEscapeUtils.unescapeJava(insertSQL));
			for (List<String> datas : setData) {
				countBatch++;
				i = 1;
				for (String s : datas) {
					ps.setString(i++, s);
				}
				ps.addBatch();
				if (countBatch % 5000 == 0) {
					log.info("executeBatch");
					ps.executeBatch();
					ps.clearBatch();
					log.info("finish executeBatch");
				}
			}
			ps.executeBatch();
			ps.clearBatch();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			 e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public Set<List<String>> selectFromTable(String selectSQL, List<Integer> indexs) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		Set<List<String>> datas = new HashSet<List<String>>();
		List<String> l = new ArrayList<String>();
		try {
			ps = this._con.prepareStatement(StringEscapeUtils.unescapeJava(selectSQL));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				for (int i : indexs) {
					if (rs.getString(i) == null) {
						l.add("");

					} else {
						l.add(rs.getString(i).toString());
					}

				}
			}
			datas.add(l);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return datas;
	}

	public Set<List<String>> selectFromTableWithParameter(String selectSQL, List<String> setData,
			List<Integer> indexs) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		Set<List<String>> datas = new HashSet<List<String>>();
		List<String> l = null;
		// List<String> l = new ArrayList<String>();
		try {
			// Set parameters into selectSQL
			ps = this._con.prepareStatement(StringEscapeUtils.unescapeJava(selectSQL));
			int i = 1;
			for (String s : setData) {
				ps.setString(i++, s);
			}
			// Execute the SQL and get response.
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				l = new ArrayList<String>();
				for (int index : indexs) {
					if (rs.getString(index) == null) {
						l.add("");

					} else {
						l.add(rs.getString(index).toString());
					}

				}
				datas.add(l);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return datas;
	}

	public void closeConnection() {
		try {
			this._con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getID() {
		// TODO Auto-generated method stub
		String selectSQL = "select max(id) from juiker_log", result = null;
		PreparedStatement ps = null;
		// List<String> l = new ArrayList<String>();
		try {
			// Set parameters into selectSQL
			ps = this._con.prepareStatement(StringEscapeUtils.unescapeJava(selectSQL));
			// Execute the SQL and get response.
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = rs.getString(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(result);
		if (result == null) {
			result = "1";
		} else {
			result = String.valueOf((Integer.valueOf(result) + 1));
		}

		return result;
	}

}
