package com.tradevan.operate;

import java.util.List;
import java.util.Set;

import com.tradevan.model.Rss;

public interface RSScrawler {
	public void run();

	void parseRssDetail(String rssSite,String category);

	void parseNewsContent(Rss rss);

	void write2Table(String insertSQL, Set<List<String>> setData);

	public boolean isRssDetailExit(Rss rss);

	public String getInfo();

	public void run(int i);

}
