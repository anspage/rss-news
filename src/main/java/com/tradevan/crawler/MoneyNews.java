package com.tradevan.crawler;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.log4j.Logger;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import com.tradevan.model.Rss;
import com.tradevan.model.TableRssProp;
import com.tradevan.operate.AccessDatabase;
import com.tradevan.operate.LogAction;
import com.tradevan.operate.RSScrawler;

public class MoneyNews extends LogAction implements RSScrawler {
	private AccessDatabase _access = new AccessDatabase();
	public Elements eachRows;
	public HashMap<String, String> eachNews = new HashMap<String, String>();

	public void run() {
		// TODO Auto-generated method stub
		String[] urls = { "https://money.udn.com/rssfeed/news/1001/5589/5603?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5589/11037?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5589/5604?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5589/11740?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5589/5605?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5589/11038?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5588/10511?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5588/5599?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5588/8887?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5588/5601?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5588/5602?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5588/9529?ch=money",
				"https://money.udn.com/rssfeed/news/1001/11111/11112?ch=money",
				"https://money.udn.com/rssfeed/news/1001/11111/11113?ch=money",
				"https://money.udn.com/rssfeed/news/1001/12017/5613?ch=money",
				"https://money.udn.com/rssfeed/news/1001/12017/5616?ch=money",
				"https://money.udn.com/rssfeed/news/1001/12017/9740?ch=money",
				"https://money.udn.com/rssfeed/news/1001/12017/6710?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/11798?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/11074?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/8044?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/5739?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/5608?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/9625?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/5607?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/10900?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/9607?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/5710?ch=money",
				"https://money.udn.com/rssfeed/news/1001/5590/9008?ch=money" };
		String[] categories = { "兩岸新聞", "兩岸新聞", "兩岸新聞", "兩岸新聞", "兩岸新聞", "兩岸新聞", "國際新聞", "國際新聞", "國際新聞", "國際新聞", "國際新聞",
				"國際新聞", "期貨新聞", "期貨新聞", "金融新聞", "金融新聞", "金融新聞", "金融新聞", "證卷新聞", "證卷新聞", "證卷新聞", "證卷新聞", "證卷新聞", "證卷新聞",
				"證卷新聞", "證卷新聞", "證卷新聞", "證卷新聞", "證卷新聞" };
		super.logger = Logger.getLogger(MoneyNews.class);
		super.logger.info("Money RSS START.");
		for (int i = 0; i < urls.length; i++) {

			try {
				parseRssDetail(urls[i], categories[i]);

			} finally {

			}
		}
		this._access.closeConnection();
		super.logger.info("Money RSS FINISHED.");
	}

	public void parseRssDetail(String rssSite, String category) {
		// TODO Auto-generated method stub
		Rss rss = new Rss();
		Document doc = null;
		String title = "", link = "", description = "", pubDate = "";
		try {
			/*---------------get web html--------------*/
			doc = Jsoup.connect(rssSite).userAgent("Mozilla").timeout(5000).get();
			Elements items = doc.select("item");
			for (Element item : items) {
				title = cleanTitle(item.select("title").text());
				pubDate = cleanTitle(item.select("pubDate").text());
				link = cleanTitle(item.select("link").text());
				description = cleanTitle(item.select("description").text());
				rss.setTitle(title);
				rss.setLink(link);
				rss.setTime(pubDate);
				rss.setP_time(pubDate);
				rss.setDescription(description);
				rss.setSource("經濟日報");
				rss.setCategory(category);
				// System.out.println(link);
				// 是否有重複的資料
				if (isRssDetailExit(rss)) {
					parseNewsContent(rss);
				} else {
					super.logger.debug("IS EXIST");
				}

			}
		} catch (

		IOException e) {
			// TODO Auto-generated catch block
			super.logger.debug("WebSitle not found...");
			// e.printStackTrace();
		} catch (Exception eAll) {
			super.logger.debug("Other error like website or firefox...");
		}
	}

	public void parseNewsContent(Rss rss) {
		// TODO Auto-generated method stub
		TableRssProp rssProp = new TableRssProp();
		Document doc = null;
		String item;
		Set<List<String>> result = null;
		List<String> l = null;
		String insertSql = String.format(
				"INSERT INTO %s( %s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?,?,?,?); ",
				rssProp.getTablename(), rssProp.getTitle(), rssProp.getContent(), rssProp.getPost_date(),
				rssProp.getUrl(), rssProp.getSource(), rssProp.getCategory(), rssProp.getFull_content(),
				rssProp.getP_date(), rssProp.getUpdate_date());
		try {
			/*---------------get web html--------------*/
			doc = Jsoup.connect(rss.getLink())
					.userAgent("Mozilla/5.0 (Windows NT 6.2; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0").timeout(5000)
					.get();
			// ndArticle_contentBox
			// #story_body_content > p
			item = doc.select("#article_body").text();
			String[] datetime = ((doc.select("#shareBar > div.shareBar__info > div > span").text())).split(" ");
			rss.setR_content_full(item);

			result = new HashSet<List<String>>();
			l = new ArrayList<String>();
			l.add(rss.getTitle());
			l.add(rss.getDescription());
			l.add(datetime[0]);
			l.add(rss.getLink());
			l.add(rss.getSource());
			l.add(rss.getCategory());
			l.add(rss.getR_content_full());
			l.add(rss.getP_time());
			l.add(rss.getUpdate_time());
			result.add(l);
			write2Table(insertSql, result);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			super.logger.debug("WebSitle not found...");
			// e.printStackTrace();
		} catch (Exception eAll) {
			super.logger.debug("Other error like website or firefox...");
		}
	}

	public void write2Table(String insertSQL, Set<List<String>> setData) {
		// TODO Auto-generated method stub
		_access.write2Table(insertSQL, setData);
	}

	public boolean isRssDetailExit(Rss rss) {
		// TODO Auto-generated method stub
		TableRssProp rssProp = new TableRssProp();
		boolean result = false;
		String selectSQL = String.format("select %s from %s where %s = ? and %s = ? ;", rssProp.getTitle(),
				rssProp.getTablename(), rssProp.getUrl(), rssProp.getPost_date());

		List<Integer> indexs = new ArrayList<Integer>();
		List<String> setData = new ArrayList<String>();
		Set<List<String>> response = null;
		setData.add(rss.getLink());
		setData.add(rss.getPubDate());
		// Set column index for response data
		indexs.add(1);

		// Send select sql
		response = this._access.selectFromTableWithParameter(selectSQL, setData, indexs);
		if (response.size() == 0) {
			result = true;
		}
		return result;
	}

	public String cleanTitle(String title) {
		// TODO Auto-generated method stub
		String[] result_1, result;
		result_1 = title.split("】");
		if (result_1.length > 1) {
			result = result_1[result_1.length - 1].split("》");
		} else {
			result = result_1[0].split("》");
		}
		if (result.length > 1) {
			return result[result.length - 1];
		} else {
			return result[0];
		}

	}

	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	public void run(int i) {
		// TODO Auto-generated method stub

	}

}
