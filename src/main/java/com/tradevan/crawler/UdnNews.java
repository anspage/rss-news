package com.tradevan.crawler;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.log4j.Logger;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import com.tradevan.model.Rss;
import com.tradevan.model.TableRssProp;
import com.tradevan.operate.AccessDatabase;
import com.tradevan.operate.LogAction;
import com.tradevan.operate.RSScrawler;

import junit.framework.Assert;

public class UdnNews extends LogAction implements RSScrawler {
	private AccessDatabase _access = new AccessDatabase();
	public Elements eachRows;
	public HashMap<String, String> eachNews = new HashMap<String, String>();

	public void run() {
		// TODO Auto-generated method stub
		String[] urls = { "https://udn.com/rssfeed/news/2/6638?ch=news", "https://udn.com/rssfeed/news/2/6639?ch=news",
				"https://udn.com/rssfeed/news/2/7225?ch=news", "https://udn.com/rssfeed/news/2/6641?ch=news",
				"https://udn.com/rssfeed/news/2/6640?ch=news" };
		String[] categories = { "政治新聞", "社會新聞", "全球新聞", "地方新聞", "兩岸新聞" };

		super.logger = Logger.getLogger(UdnNews.class);
		for (int i = 0; i < urls.length; i++) {

			try {
				super.logger.info(categories[i]);
				parseRssDetail(urls[i], categories[i]);
			} finally {

			}
		}

		this._access.closeConnection();
		super.logger.info("Udn RSS FINISHED.");
	}

	public void parseRssDetail(String rssSite, String category) {
		// TODO Auto-generated method stub
		Rss rss = new Rss();
		Document doc = null;
		String title = "", link = "", description = "", pubDate = "";

		try {
			/*---------------get web html--------------*/
			doc = Jsoup.connect(rssSite).userAgent("Mozilla").timeout(5000).get();
			Elements items = doc.select("item");
			for (Element item : items) {
				title = cleanTitle(item.select("title").text());
				pubDate = cleanTitle(item.select("pubDate").text());
				link = cleanTitle(item.select("link").text());
				description = cleanTitle(item.select("description").text());
				rss.setTitle(title);
				rss.setLink(link);
				rss.setTime(pubDate);
				rss.setP_time(pubDate);
				rss.setDescription(description);
				rss.setSource("聯合新聞網");
				rss.setCategory(category);
				// 是否有重複的資料
				if (isRssDetailExit(rss)) {
					parseNewsContent(rss);
				} else {
					super.logger.debug("IS EXIST");
				}
			}

		} catch (

		IOException e) {
			// TODO Auto-generated catch block
			super.logger.debug("WebSitle not found...");
			e.printStackTrace();
		} catch (Exception eAll) {
			super.logger.debug("Other error like website or firefox...");
			eAll.printStackTrace();
		}
	}

	public void parseNewsContent(Rss rss) {
		// TODO Auto-generated method stub
		Document doc = null;
		String item;
		// 寫入語法
		Set<List<String>> result = null;
		TableRssProp rssProp = new TableRssProp();
		String insertSql = String.format(
				"INSERT INTO %s( %s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?,?,?,?); ",
				rssProp.getTablename(), rssProp.getTitle(), rssProp.getContent(), rssProp.getPost_date(),
				rssProp.getUrl(), rssProp.getSource(), rssProp.getCategory(), rssProp.getFull_content(),
				rssProp.getP_date(), rssProp.getUpdate_date());

		List<String> l = null;
		try {
			/*---------------get web html--------------*/
			doc = Jsoup.connect(rss.getLink())
					.userAgent("Mozilla/5.0 (Windows NT 6.2; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0").timeout(5000)
					.get();

			// ndArticle_contentBox
			item = doc.select("#story_body_content > p").text();
			rss.setR_content_full(item);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			super.logger.debug("WebSitle not found... " + rss.getLink());
			e.printStackTrace();
		} catch (Exception eAll) {
			super.logger.debug("Other error like website or firefox...");
		}
		logger.debug(rss.getTitle());
		// WRITE INTO TABLE
		result = new HashSet<List<String>>();
		l = new ArrayList<String>();
		l.add(rss.getTitle());
		l.add(rss.getDescription());
		l.add(rss.getPubDate());
		l.add(rss.getLink());
		l.add(rss.getSource());
		l.add(rss.getCategory());
		l.add(rss.getR_content_full());
		l.add(rss.getP_time());
		l.add(rss.getUpdate_time());
		result.add(l);
		// super.logger.info(l);
		// 寫入DB
		write2Table(insertSql, result);
	}

	public void write2Table(String insertSQL, Set<List<String>> setData) {
		// TODO Auto-generated method stub
		_access.write2Table(insertSQL, setData);
	}

	public boolean isRssDetailExit(Rss rss) {
		// TODO Auto-generated method stub
		TableRssProp rssProp = new TableRssProp();
		boolean result = false;
		String selectSQL = String.format("select %s from %s where %s = ? and %s = ? ;", rssProp.getTitle(),
				rssProp.getTablename(), rssProp.getUrl(), rssProp.getPost_date());

		List<Integer> indexs = new ArrayList<Integer>();
		List<String> setData = new ArrayList<String>();
		Set<List<String>> response = null;
		setData.add(rss.getLink());
		setData.add(rss.getPubDate());
		// Set column index for response data
		indexs.add(1);

		// Send select sql
		response = this._access.selectFromTableWithParameter(selectSQL, setData, indexs);
		if (response.size() == 0) {
			result = true;
		}
		return result;
	}

	public String cleanTitle(String title) {
		// TODO Auto-generated method stub
		String[] result_1, result;
		result_1 = title.split("】");
		if (result_1.length > 1) {
			result = result_1[result_1.length - 1].split("》");
		} else {
			result = result_1[0].split("》");
		}
		if (result.length > 1) {
			return result[result.length - 1];
		} else {
			return result[0];
		}

	}

	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	public void run(int i) {
		// TODO Auto-generated method stub

	}

}
