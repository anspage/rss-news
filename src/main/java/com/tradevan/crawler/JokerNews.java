package com.tradevan.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.select.Elements;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tradevan.model.Rss;
import com.tradevan.model.TableRssProp;
import com.tradevan.operate.AccessDatabase;
import com.tradevan.operate.LogAction;
import com.tradevan.operate.RSScrawler;

import org.apache.log4j.Logger;

public class JokerNews extends LogAction implements RSScrawler {
	private AccessDatabase _access = new AccessDatabase();
	public Elements eachRows;

	public void run() {
		// TODO Auto-generated method stub
		String startDate = getYDateString(lastdate(1));
		String endDate = getYDateString(lastdate(1));
		String url = String.format("http://192.168.0.104/eypocapi/json/10/?$startDate=%s&$endDate=%s", startDate,
				endDate);
		// String url = String.format("http://127.0.0.1/N333/text.json");
		super.logger = Logger.getLogger(JokerNews.class);
		try {
			parseRssDetail(url, "", startDate);

		} finally {

		}

		this._access.closeConnection();
		super.logger.info("Joker RSS FINISHED.");
	}

	public void run(int i) {
		// TODO Auto-generated method stub
		String startDate = getYDateString(lastdate(i));
		String endDate = getYDateString(lastdate(i));
		String url = String.format("http://192.168.0.104/eypocapi/json/10/?$startDate=%s&$endDate=%s", startDate,
				endDate);

		super.logger = Logger.getLogger(JokerNews.class);
		try {
			parseRssDetail(url, "", startDate);

		} finally {

		}

		this._access.closeConnection();
		super.logger.info("Joker RSS FINISHED.");
	}

	public void parseRssDetail(String rssSite, String category, String date) {
		// TODO Auto-generated method stub
		InputStream is = null;
		try {
			is = new URL(rssSite).openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd), jsonResult = null, jsonSEQNO = null, startDate = null, endDate = null,
					jsonDate = null, id = null, status = null, source = null;
			JsonArray arrayJosn = new JsonParser().parse(jsonText).getAsJsonArray();
			JsonObject obj = null;
			java.sql.Timestamp sqlTimestamp = null;
			Set<List<String>> result = null;
			;
			List<String> l = null;
			// String insertSql = String.format(
			// "INSERT INTO juiker_log( id, SEQNO, source, cnt, status, post_date,
			// start_time, end_time) VALUES (?,?,?,?,?,?,?,?); ");

			// jsonText = fullWidth2halfWidth(jsonText);
			for (int i = 0; i < arrayJosn.size(); i++) {
				result = new HashSet<List<String>>();
				sqlTimestamp = new java.sql.Timestamp(System.currentTimeMillis());

				obj = (JsonObject) arrayJosn.get(i);
				jsonSEQNO = obj.get("SEQNO").toString();
				jsonSEQNO = jsonSEQNO.replace("\"", "");
				jsonResult = obj.get("內容").toString();
				jsonDate = obj.get("日期").toString();
				jsonDate = jsonDate.substring(1, 11);
				if (jsonResult.contains("行政院新聞傳播處")) {
					source = "行政院新聞傳播處";
				} else if (jsonResult.contains("潤利公司")) {
					source = "潤利公司";
				} else if (jsonResult.contains("網友意見摘要表")) {
					continue;
				} else {
					source = "其他";
				}
				startDate = sqlTimestamp.toString();
				this.logger.debug(startDate);
				int count = cutNewsData(jsonResult, jsonDate);
				if (count > 0) {
					status = "Success";
				} else {
					status = "Error";
				}
				sqlTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
				endDate = sqlTimestamp.toString();
				this.logger.debug(endDate);
				id = _access.getID();
				l = new ArrayList<String>();
				l.add(id);
				l.add(jsonSEQNO);
				l.add(source);
				l.add(String.valueOf(count));
				l.add(status);
				l.add(jsonDate);
				l.add(startDate);
				l.add(endDate);
				super.logger.info(l);
				result.add(l);
				// write2Table(insertSql, result);
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// } catch (Exception e) {
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int cutNewsData(String jsonText, String date) {
		jsonText = fullWidth2halfWidth(jsonText);
		Rss rss = new Rss();
		String[] eachItem = jsonText.split("院新聞處");
		for (int i = 1; i < eachItem.length; i++) {
			String Content = eachItem[i];
			String draftSource = eachItem[i].substring(0, 10).replaceAll("【|,|，|/*", "@");
			String[] draftSource1 = draftSource.split("@");
			String time[] = eachItem[i - 1].split(":");
			try {
				String Hour = time[time.length - 2];
				String Min = time[time.length - 1];
				String getTime = Hour.substring(Hour.length() - 2, Hour.length()) + ":" + Min.substring(0, 2);
				rss.setTime(date + " " + getTime);
			} catch (Exception e) {
			}

			// rss.setTime(time[time.length - 2]);
			String[] draftUrl1 = null;
			String[] draftUrl2 = null;
			String url = null;
			String Source = "";
			String title = "", link = "";
			if (draftSource1.length > 1) {
				Source = draftSource1[0];
				title = Content.substring(Source.length() + 1);
				String[] ContentArr = title.split("。");
				this.logger.debug(ContentArr[0]);
				this.logger.debug(ContentArr[ContentArr.length - 1]);
				for (int cnt = 0; cnt < ContentArr.length - 1; cnt++) {
					if (cnt == 0) {
						title = ContentArr[cnt] + "。";
					} else {
						title = title + ContentArr[cnt] + "。";
					}
				}
				this.logger.debug(title);
				// title.replace(Content[Content.length-1], "");
			}
			draftUrl1 = eachItem[i].split("http");
			if (draftUrl1.length > 1) {
				draftUrl2 = draftUrl1[1].split(" ");
				url = draftUrl2[0].substring(0, 17);
				link = "http" + url;
			}
			// super.logger.info(title);
			rss.setTitle(title);
			rss.setLink(link);
			rss.setDescription(title);
			rss.setSource("揪科");
			if (Source.length() > 0) {
				rss.setCategory(Source.substring(1));
			}
			if (title.length() < 600 && title.length() > 50 && !title.contains("{")) {
				if (isRssDetailExit(rss)) {
					parseNewsContent(rss);
				} else {
					System.out.println("[INFO] IS EXIST");
				}
			}

		}
		return eachItem.length - 1;

	}

	public void parseNewsContent(Rss rss) {
		// TODO Auto-generated method stub
		TableRssProp rssProp = new TableRssProp();
		Set<List<String>> result = null;
		List<String> l = null;
		String insertSql = String.format("INSERT INTO %s( %s, %s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?,?); ",
				rssProp.getTablename(), rssProp.getTitle(), rssProp.getContent(), rssProp.getPost_date(),
				rssProp.getUrl(), rssProp.getSource(), rssProp.getCategory(), rssProp.getFull_content());

		String date = getYDateFormate(lastdate(1));

		rss.setR_content_full("");

		if (rss.getTitle().length() > 20) {
			result = new HashSet<List<String>>();
			l = new ArrayList<String>();
			l.add(rss.getTitle());
			l.add(rss.getDescription()); 
			l.add(rss.getPubDate());
			l.add(rss.getLink());
			l.add(rss.getSource());
			l.add(rss.getCategory());
			l.add(rss.getR_content_full());
			// super.logger.info(l);
			result.add(l);
			write2Table(insertSql, result);
		}

	}

	public void write2Table(String insertSQL, Set<List<String>> setData) {
		// TODO Auto-generated method stub
		_access.write2Table(insertSQL, setData);
	}

	public boolean isRssDetailExit(Rss rss) {
		// TODO Auto-generated method stub
		TableRssProp rssProp = new TableRssProp();
		boolean result = false;
		String date = getYDateFormate(lastdate(1));

		String selectSQL = String.format("select %s from %s where %s = ? and %s = ? and %s = ? ;", rssProp.getTitle(),
				rssProp.getTablename(), rssProp.getTitle(), rssProp.getPost_date(), rssProp.getSource());
		List<Integer> indexs = new ArrayList<Integer>();

		this.logger.info(selectSQL);
		List<String> setData = new ArrayList<String>();
		Set<List<String>> response = null;
		setData.add(rss.getTitle());
		setData.add(rss.getPubDate());
		setData.add(rss.getSource());
		// Set column index for response data
		indexs.add(1);

		// this.logger.info(setData);

		// Send select sql
		response = this._access.selectFromTableWithParameter(selectSQL, setData, indexs);
		if (response.size() == 0) {
			result = true;
		}

		return result;
	}

	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	private Date lastdate(Integer i) {
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -i);
		return cal.getTime();
	}

	private String getYDateFormate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(date);
	}

	private String getYDateString(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(date);
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	private static String fullWidth2halfWidth(String fullWidthStr) {
		if (null == fullWidthStr || fullWidthStr.length() <= 0) {
			return "";
		}
		char[] charArray = fullWidthStr.toCharArray();
		// 對全形字元轉換的char陣列遍歷
		for (int i = 0; i < charArray.length; i++) {
			int charIntValue = (int) charArray[i];
			// 如果符合轉換關係,將對應下標之間減掉偏移量65248;如果是空格的話,直接做轉換
			if (charIntValue >= 65281 && charIntValue <= 65374) {
				charArray[i] = (char) (charIntValue - 65248);
			} else if (charIntValue == 12288) {
				charArray[i] = (char) 32;
			}
		}
		return new String(charArray);
	}

	public void parseRssDetail(String rssSite, String category) {
		// TODO Auto-generated method stub

	}
}
