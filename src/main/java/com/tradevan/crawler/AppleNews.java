package com.tradevan.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.log4j.Logger;

import com.tradevan.model.Rss;
import com.tradevan.model.TableRssProp;
import com.tradevan.operate.AccessDatabase;
import com.tradevan.operate.LogAction;
import com.tradevan.operate.RSScrawler;

public class AppleNews extends LogAction implements RSScrawler {
	private AccessDatabase _access = new AccessDatabase();
	public Elements eachRows;
	public void run() {
		// TODO Auto-generated method stub
		String url = "https://tw.appledaily.com/rss/newcreate/kind/rnews/type/new";
		super.logger = Logger.getLogger(AppleNews.class);
		try {
			parseRssDetail(url,"");
			super.logger.info("Apple RSS FINISHED.");
		} finally {
			this._access.closeConnection();
		}
	}

	public void parseRssDetail(String rssSite,String category) {
		// TODO Auto-generated method stub
		Rss rss = new Rss();
		Document doc = null;
		String title = "", link = "", description = "";
		try {
			/*---------------get web html--------------*/
			doc = Jsoup.connect(rssSite).get();
			Elements items = doc.select("item");

			for (Element item : items) {
				title = cleanTitle(item.select("title").text());
				// pubDate = cleanTitle(item.select("pubDate").text());
				link = cleanTitle(item.select("link").text());
				description = cleanTitle(item.select("description").text());
				rss.setTitle(title);
				rss.setLink(link);
				rss.setDescription(description);
				rss.setSource("蘋果新聞");
				parseNewsContent(rss);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			super.logger.debug("WebSitle not found...");
			// e.printStackTrace();
		} catch (Exception eAll) {
			super.logger.debug("Other error like website or firefox...");
		}
	}

	public void parseNewsContent(Rss rss) {
		// TODO Auto-generated method stub
		TableRssProp rssProp = new TableRssProp();
		Document doc = null;
		String item;
		Set<List<String>> result = null;
		List<String> l = null;
		String insertSql = String
				.format("INSERT INTO %s( %s, %s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?,?); ",
						rssProp.getTablename(), rssProp.getTitle(),
						rssProp.getContent(), rssProp.getPost_date(),
						rssProp.getUrl(), rssProp.getSource(),
						rssProp.getCategory(), rssProp.getFull_content());
		try {
			/*---------------get web html--------------*/
			doc = Jsoup.connect(rss.getLink()).get();
			// ndArticle_contentBox
			item = doc.getElementsByClass("ndArticle_margin").text();
			rss.setR_content_full(item);

			if (isRssDetailExit(rss)) {
				result = new HashSet<List<String>>();
				l = new ArrayList<String>();
				l.add(rss.getTitle());
				l.add(rss.getDescription());
				l.add(rss.getPubDate());
				l.add(rss.getLink());
				l.add(rss.getSource());
				l.add(rss.getCategory());
				l.add(rss.getR_content_full());
				System.out.println(l);
				result.add(l);
				write2Table(insertSql, result);
			}else{
				System.out.println("[INFO] IS EXIST");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			super.logger.debug("WebSitle not found...");
			// e.printStackTrace();
		} catch (Exception eAll) {
			super.logger.debug("Other error like website or firefox...");
		}
	}

	public void write2Table(String insertSQL, Set<List<String>> setData) {
		// TODO Auto-generated method stub
		_access.write2Table(insertSQL, setData);
	}

	public boolean isRssDetailExit(Rss rss) {
		// TODO Auto-generated method stub
		TableRssProp rssProp = new TableRssProp();
		boolean result = false;
		String selectSQL = String.format(
				"select %s from %s where %s = ? and %s = ? and %s = ? ;",
				rssProp.getTitle(), rssProp.getTablename(), rssProp.getTitle(),
				rssProp.getUrl(), rssProp.getPost_date());

		List<Integer> indexs = new ArrayList<Integer>();
		List<String> setData = new ArrayList<String>();
		Set<List<String>> response = null;
		setData.add(rss.getTitle());
		setData.add(rss.getLink());
		setData.add(rss.getPubDate());
		// Set column index for response data
		indexs.add(1);

		// Send select sql
		response = this._access.selectFromTableWithParameter(selectSQL,
				setData, indexs);
		if (response.size() == 0) {
			result = true;
		}
		return result;
	}

	public String cleanTitle(String title) {
		// TODO Auto-generated method stub
		String[] result;
		result = title.split("】");
		if (result.length > 1) {
			return result[result.length-1];
		} else {
			return result[0];
		}

	}

	public String getInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	public void run(int i) {
		// TODO Auto-generated method stub
		
	}

}
